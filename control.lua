local handler = require "event_handler"


local on_research_finished = function (event)
   if not event.by_script then
      local research = event.research
      local local_name = research.localised_name
      for _, player in pairs (research.force.players) do
         if player.valid then
            player.print ({"chat.finished-research", local_name})
         end
      end
   end
end



local lib = {}


lib.events = {
   [defines.events.on_research_finished] = on_research_finished,
}


-- Called on new game.
-- Called when added to exiting game.
lib.on_init = function()
end


-- Not called when added to existing game.
-- Called when loaded after saved in existing game.
lib.on_load = function()
end


-- Not called on new game.
-- Called when added to existing game.
lib.on_configuration_changed = function (config)
end


-- Register the events.
handler.add_lib (lib)
